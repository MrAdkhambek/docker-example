package mr.adkhambek.docker.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1")
public class TestController {

    @GetMapping("/test")
    public ResponseEntity<?> testGET() {
        return ResponseEntity.ok("Test success get");
    }

    @PostMapping("/test")
    public ResponseEntity<?> testPOST(@RequestBody DataModel body) {
        return ResponseEntity.ok(body);
    }

    @PutMapping("/test")
    public ResponseEntity<?> testPUT(@RequestBody DataModel body) {
        return ResponseEntity.ok(body);
    }

    @DeleteMapping("/test")
    public ResponseEntity<?> testDELETE() {
        return ResponseEntity.ok("Test success delete");
    }
}
