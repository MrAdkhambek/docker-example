package mr.adkhambek.docker.controller;

import lombok.Data;

@Data
public class DataModel {
    private String data;
}
