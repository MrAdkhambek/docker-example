package mr.adkhambek.docker.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestControllerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGET() {
        assertEquals(5,5);
    }

    @Test
    void testPOST() {
        assertEquals(5,5);
    }

    @Test
    void testPUT() {
        assertEquals(5,5);
    }

    @Test
    void testDELETE() {
        assertEquals(5,4);
    }
}